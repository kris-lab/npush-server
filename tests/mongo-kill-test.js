/*
 * Mongo Kill Test
 */

var cluster = require('cluster');
var http = require('http');
var os = require('os')
var numCPUs = os.cpus().length;

var databaseUrl = "ni_sockets"
, db = require("mongojs").connect(databaseUrl)
, Connection = require('cassandra-client').Connection
, con = new Connection({host:'localhost', port:9160, keyspace:'DEMO'});

if (cluster.isMaster) {
	
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
	
	console.log(cluster.workers);
  });
  
	console.log(cluster.workers);
  
} else {
  
	var start = (new Date()).getTime();
	
	console.log(cluster.worker.id);
	console.log(process.pid);
	
	/*
	if(0) {
		
		con.connect(function(err) {
			if (err) {
				throw err;
			} else {

				var msges = 0;

				while(msges < 50000) {

					con.execute('INSERT INTO users (key, name) VALUES (?, ?)', [msges, msges],
						function (err) {
							if (err) {
							throw err;
							} else {
								//console.log('success!');
							}


						});	

						msges++;
				}

				console.log(((new Date()).getTime() - start) + ' ms');
			}
		});	
  
	} else {

		var msges = 0;
		var lock = false;

		while(msges < 50000) {

			db.collection('mongotest').save({t:1});
			msges++;
		}

		console.log(((new Date()).getTime() - start) + ' ms');  
	}
	*/
   
   process.exit();
}

/*
* 

COMMUNICATION BETWEEN MASTER AND SLAVE

var cluster = require('cluster');
var http = require('http');
var numReqs = 0;
var worker;

if (cluster.isMaster) {
  // Fork workers.
  for (var i = 0; i < 2; i++) {
    worker = cluster.fork();

    worker.on('message', function(msg) {
      // we only want to intercept messages that have a chat property
      if (msg.chat) {
        console.log('Worker to master: ', msg.chat);
        worker.send({ chat: 'Ok worker, Master got the message! Over and out!' });
      }
    });

  }
} else {
  process.on('message', function(msg) {
    // we only want to intercept messages that have a chat property
    if (msg.chat) {
      console.log('Master to worker: ', msg.chat);
    }
  });
  // Worker processes have a http server.
  http.Server(function(req, res) {
    res.writeHead(200);
    res.end("hello world\n");
    // Send message to master process
    process.send({ chat: 'Hey master, I got a new request!' });
  }).listen(8000);
}






*/